#!/bin/bash
echo "Running pre-commit hook" 
echo "Running unittests"
./project/scripts/run-unittests.sh
#coverage run --omit */test_*,*__init__.py -m unittest discover -s project -v


if [ $? -ne 0 ]; then
	echo "Unit tests must pass before commit"
	exit 1
fi

echo "unittests succeeded"

